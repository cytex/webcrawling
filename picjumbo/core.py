import logging
from bs4 import BeautifulSoup
import requests
from urllib import parse
import asyncio
from aiohttp import ClientSession


logger = logging.getLogger(__name__)
start_url = 'https://picjumbo.com/latest-free-stock-photos/'
page_url = 'https://picjumbo.com/latest-free-stock-photos/page/{0}/'
base_url = 'https://picjumbo.com'


def get_html(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    soup.prettify()
    return soup


class PicJumbo(object):

    @staticmethod
    def start_download():
        logger.info('Start downloading')
        for page in PicJumbo.get_urls():
            soup = get_html(page)
            links_to_images = soup.find_all("div", "query_item_wrap")
            links = [container.find('a', href=True)['href'] for container in links_to_images]
            link_parts = [links[i:i + 6] for i in range(0, len(links), 6)]
            for part in link_parts:
                loop = asyncio.get_event_loop()
                tasks = [asyncio.ensure_future(PicJumbo.download_images(x)) for x in part]
                loop.run_until_complete(asyncio.wait(tasks))

    @staticmethod
    async def download_images(url):
        async with ClientSession() as session:
            async with session.get(url) as response:
                r = await response.read()
                predownload_page = BeautifulSoup(r, 'html.parser')
                predownload_page.prettify()
                predownload_link = '{0}{1}'.format(
                    base_url, predownload_page.find('a', 'button big', href=True)['href']
                )
                async with session.get(predownload_link) as response2:
                    r = await response2.read()
                    download_page = BeautifulSoup(r, 'html.parser')
                    download_page.prettify()
                    download_link = download_page.find(
                        'div', 'downloading'
                    ).find('a')['href']
                    parsed = parse.parse_qs(parse.urlsplit(download_link).query)
                    logging.info('start download {}'.format(parsed['d'][0]))

                    async with session.get(download_link) as response3:
                        with open('downloads/{}'.format(parsed['d'][0]), 'wb') as out_file:
                            r = await response3.read()
                            out_file.write(r)
                            logging.info('downloaded {}'.format(parsed['d'][0]))
                        del response

    @staticmethod
    def get_urls():
        soup = get_html(start_url)
        page_numbers = soup.find_all("a", "page-numbers")
        last_page = page_numbers[-2].text
        urls = [page_url.format(x+1) for x in range(int(last_page))]
        urls[0] = start_url
        return urls
