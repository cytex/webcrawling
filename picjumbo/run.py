from core import PicJumbo
import logging
import logging.config
import yaml

with open("logging.yaml", 'r') as the_file:
    config_dict = yaml.load(the_file)

logging.config.dictConfig(config_dict)

if __name__ == "__main__":
    PicJumbo.start_download()
